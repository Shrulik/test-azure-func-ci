#r "Newtonsoft.Json"

using System;
using System.Net;
using Newtonsoft.Json;

using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;


static RegistryManager registryManager;

public static async Task<object> Run(HttpRequestMessage req, string id, TraceWriter log)
{
    log.Info($"Device registration was called !");

     string connectionString = "HostName=CityZenPocHub.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=pDHzXvc1ogzBEdKfgKjJBeqpFzmKz62+WeuCfk6qj0A=";

     registryManager = RegistryManager.CreateFromConnectionString(connectionString);

     var devicePKey = await AddDeviceAsync(id);

    //This log could be a lie actually
    log.Info("Generated device key: " + devicePKey);

    return req.CreateResponse(HttpStatusCode.OK, new {
        pkey = devicePKey
    });
}

 private static async Task<string> AddDeviceAsync(string id)
{
            string deviceId = "rest-device-" + id;
            Device device;
            try
            {
                device = await registryManager.AddDeviceAsync(new Device(deviceId));
            }
            catch (DeviceAlreadyExistsException)
            {
                device = await registryManager.GetDeviceAsync(deviceId);
            }
            
            return device.Authentication.SymmetricKey.PrimaryKey;
}
