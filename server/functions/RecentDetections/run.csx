#r "Microsoft.WindowsAzure.Storage"

using Microsoft.WindowsAzure.Storage.Table;

using System.Net;
using System.Linq;

public static HttpResponseMessage Run(HttpRequestMessage req,
IQueryable<Detection> recDetectionsTable, 
TraceWriter log)
{
    log.Info("C# HTTP trigger function processed a request.");


    // Get request body
    //dynamic data = await req.Content.ReadAsAsync<object>();

    // Set name to query string or body data
   // name = name ?? data?.name;


    return req.CreateResponse(HttpStatusCode.OK,new {
        detectionTypes = new String[]{"Fire", "Murder", "Party"},
        detections = Queryable.Take(recDetectionsTable, 100).ToList()             
    });
}



public class Detection : TableEntity {
     public string deviceId;
     public string detectionId;
     public string type;
     public string  desc ;
}