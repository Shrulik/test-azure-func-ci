#r "Newtonsoft.Json"

using System.Net;

using Newtonsoft.Json;


public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, TraceWriter log)
{
   string data2 =  @"
    {
        'messageId': '58b71454932bb756e7c94874',
        'datetime': 1415160551342,
        'userId': '58b714544ad9d1f387f15074',
        'content': 'dolor ea duis cupidatat eu nulla ex irure consectetur consequat'
      }
  ";
  string data = @"[
  {
    'conversationId': '58b714547fbca76c46b5ed36',
    'updateDate': 1475921615058,
    'createdDate': 1481710448092,
    'messages': [
      {
        'messageId': '58b71454932bb756e7c94874',
        'datetime': 1415160551342,
        'userId': '58b714544ad9d1f387f15074',
        'content': 'dolor ea duis cupidatat eu nulla ex irure consectetur consequat'
      },
      {
        'messageId': '58b7145420c5be11b3d912e3',
        'datetime': 1466742794458,
        'userId': '58b71454365cda088f23ce12',
        'content': 'et duis enim reprehenderit veniam ullamco id ex excepteur sunt'
      },
      {
        'messageId': '58b714546bd574cc0cc77dd4',
        'datetime': 1475576410420,
        'userId': '58b714549fbbef91ec20d06e',
        'content': 'eiusmod laborum occaecat esse ut tempor Lorem cillum consectetur irure'
      }
    ]
  },
  {
    'conversationId': '58b71454e75ea7176c6b61a1',
    'updateDate': 1465220887207,
    'createdDate': 1455867536621,
    'messages': [
      {
        'messageId': '58b7145435eb560c84f9b6c2',
        'datetime': 1451630924115,
        'userId': '58b71454de2f3e4dc4744a22',
        'content': 'cupidatat tempor non cillum eu in do ipsum non deserunt'
      },
      {
        'messageId': '58b7145430204822be4d5e7e',
        'datetime': 1431638672375,
        'userId': '58b714549c3cc9cd69d5178e',
        'content': 'ea fugiat id qui ex ea consectetur enim quis proident'
      }
    ]
  },
  {
    'conversationId': '58b714544bf5ed5453d20b7d',
    'updateDate': 1485238348153,
    'createdDate': 1467097664956,
    'messages': [
      {
        'messageId': '58b7145431a08190f393e88a',
        'datetime': 1408268219587,
        'userId': '58b714543eb8ca40963839c6',
        'content': 'enim ex cillum cupidatat qui ea quis fugiat in do'
      },
      {
        'messageId': '58b7145430a680915be348d4',
        'datetime': 1426293093268,
        'userId': '58b7145450f8441bed96fc8e',
        'content': 'esse aliquip nisi ea commodo proident consequat esse reprehenderit veniam'
      }
    ]
  }]";

    log.Info(JsonConvert.DeserializeObject(data).GetType().ToString());
    log.Info(JsonConvert.DeserializeObject(data2).GetType().ToString());
    
    //Test CI integration
    
    //TODO: For some reason, returning JObject/JArray will work in the Test environment here, but fail publicly. 
    return req.CreateResponse(HttpStatusCode.OK, new {
        conversations = JsonConvert.DeserializeObject(data)
   });

}
