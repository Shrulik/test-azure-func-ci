#r "Newtonsoft.Json"

using System;
using Microsoft.Azure.Devices;
using Newtonsoft.Json;
using System.Text;

public static void Run(string myEventHubMessage, TraceWriter log)
{     
     try {
           var NewDetection = 
           Newtonsoft.Json.JsonConvert.DeserializeObject<Detection>(myEventHubMessage);

        log.Info($"NewDetection recieved: {NewDetection.deviceId} - {NewDetection.desc}");
           
     }catch (Exception ex){
        log.Error($"Error deserializing: {ex.Message}");
     }
    // log.Info($"C# Event Hub trigger function processed a message: {myEventHubMessage}");
}




public class Detection {
     public string deviceId;
     public string reportType;
     public string  desc ;
}