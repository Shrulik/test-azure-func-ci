#r "Newtonsoft.Json"

using System;
using System.Net;
using Newtonsoft.Json;

public static async Task<object> Run(HttpRequestMessage req, string id,
                                        string loc, TraceWriter log)
{
    log.Info($"Webhook was triggered!");

   

    //dynamic data = JsonConvert.DeserializeObject(jsonContent);

    var data = new {
        id ="la la",
        loc = " na na"
    };

    if (id == null || loc == null) {
        return req.CreateResponse(HttpStatusCode.BadRequest, new {
            error = "Please pass id/loc properties in the input object"
        });
    };

    //if ( !loc.Equals("Mexico-SanJuarez") )
        //return req.CreateResponse(HttpStatusCode.Forbidden);

    return req.CreateResponse(HttpStatusCode.OK, new {
        detectionTypes = new String[]{"Fire", "Murder", "Party"},
        loc = loc,
        id = id        
    });
}
